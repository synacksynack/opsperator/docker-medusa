# k8s Medusa

Medusa image.

Build with:

```
$ make build
```

Test with:

```
$ make syndemo
```

Start Demo in OpenShift:

```
$ make ocdemo
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name      |    Description             | Default                  |
| :-------------------- | -------------------------- | ------------------------ |
|  `ADMIN_PASS`         | Medusa Admin Password      | `secret`                 |
|  `ADMIN_USER`         | Medusa Admin Username      | `medusa`                 |
|  `API_KEY`            | Medusa API Key             | generated during startup |
|  `DISABLE_AUTH`       | Disables Medusa Auth       | undef                    |
|  `DOWNLOAD_URL`       | Downloads URL              | undef                    |
|  `NEWZNAB_API_KEY`    | Newznab API Key            | undef                    |
|  `NEWZNAB_HOST`       | Newznab Hostname           | `newznab`                |
|  `NEWZNAB_PORT`       | Newznab Port               | undef                    |
|  `NEWZNAB_PROTO`      | Newznab Proto              | `http`                   |
|  `SABNZB_API_KEY`     | SABnzbd API Key            | undef                    |
|  `SABNZB_HOSTNAME`    | SABnzbd Hostname           | `sabnzbd`                |
|  `SABNZB_PORT`        | SABnzbd Port               | undef                    |
|  `SABNZB_PROTO`       | SABnzbd Proto              | `http`                   |
|  `SLACK_HOOK_URL`     | Slack notifications URL    | undef                    |
|  `SMTP_FROM`          | Medusa SMTP From           | `medusa@demo.local`      |
|  `SMTP_PORT`          | Medusa SMTP Port           | `25`                     |
|  `SMTP_RELAY`         | Medusa SMTP Relay          | undef                    |
|  `SMTP_TO`            | Medusa SMTP Recipient      | `changeme@demo.local`    |
|  `TRANSMISSION_HOST`  | Transmission RPC Host      | undef                    |
|  `TRANSMISSION_PASS`  | Transmission RPC Password  | undef                    |
|  `TRANSMISSION_PORT`  | Transmission RPC Port      | undef, depends on proto  |
|  `TRANSMISSION_PROTO` | Transmission RPC Proto     | `http`                   |
|  `TRANSMISSION_USER`  | Transmission RPC Username  | undef                    |

You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point | Description           |
| :------------------ | --------------------- |
|  `/anime`           | Medusa Anime          |
|  `/config`          | Medusa Configuration  |
|  `/download`        | Medusa Downloads      |
|  `/tv`              | Medusa TV             |
