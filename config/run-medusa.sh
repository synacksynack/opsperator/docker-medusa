#!/bin/sh

DO_DEBUG=0
if test "$DEBUG"; then
    set -x
    DO_DEBUG=1
fi

. /usr/local/bin/reset-tls.sh

if ! test -s /config/config.ini; then
    ADMIN_PASS="${ADMIN_PASS:-secret}"
    ADMIN_USER=${ADMIN_USER:-medusa}
    SABMETHOD=blackhole
    DO_NZB=0
    DO_SAB=0
    DO_SMTP=0
    DO_SLACK=0
    DO_TORRENT=1
    SABURL=
    TXMETHOD=blackhole
    TXURL=
    if test "$DISABLE_AUTH"; then
	ADMIN_PASS=
	ADMIN_USER=
    fi
    if test "$NEWZNAB_API_KEY"; then
	DO_NZB=1
	PROVIDERS=kubenews
    fi
    if test "$SABNZB_API_KEY"; then
	DO_NZB=1
	DO_SAB=1
	SABMETHOD=sabnzbd
	SABNZB_HOST=${SABNZB_HOST:-sabnzbd}
	SABNZB_PROTO=${SABNZB_PROTO:-http}
	if test "$SABNZB_PORT"; then
	    SABPORT=:$SABNZB_PORT
	fi
	SABURL=$SABNZB_PROTO://$SABNZB_HOST$SABPORT
    fi
    if test "$TRANSMISSION_USER" -o "$TRANSMISSION_HOST"; then
	DO_TORRENT=1
	TRANSMISSION_HOST=${TRANSMISSION_HOST:-transmission}
	TRANSMISSION_PROTO=${TRANSMISSION_PROTO:-http}
	TXMETHOD=torrent
	if test "$TRANSMISSION_PORT"; then
	    TXPORT=:$TRANSMISSION_PORT
	fi
	TXURL=$TRANSMISSION_PROTO://$TRANSMISSION_HOST$TXPORT
    fi
    if test "$SLACK_HOOK_URL"; then
	DO_SLACK=1
	if ! echo "$SLACK_HOOK_URL" | grep hooks.slack.com >/dev/null; then
	    SLACK_HOOK_URL=https://hooks.slack.com/services/$SLACK_HOOK_URL
	fi
    fi
    if test "$SMTP_RELAY"; then
	DO_SMTP=1
	if test -z "$SMTP_TO"; then
	    SMTP_TO=changeme@demo.local
	fi
	if test -z "$SMTP_FROM"; then
	    SMTP_FROM=medusa@demo.local
	fi
    fi
    sed -e 's|web_port .*|web_port = 8080|' \
	-e "s|ADMIN_USER|$ADMIN_USER|" \
	-e "s|ADMIN_PASSWORD|$ADMIN_PASS|" \
	-e "s|API_KEY|$API_KEY|" \
	-e "s|DLURL|$DOWNLOAD_URL|" \
	-e "s|DO_DEBUG|$DO_DEBUG|" \
	-e "s|DO_NZB|$DO_NZB|" \
	-e "s|DO_SAB|$DO_SAB|" \
	-e "s|DO_SLACK|$DO_SLACK|" \
	-e "s|DO_SMTP|$DO_SMTP|" \
	-e "s|DO_TORRENT|$DO_TORRENT|" \
	-e "s|PROVIDERS|$PROVIDERS|" \
	-e "s|PROXYSETTINGS|$HTTP_PROXY|" \
	-e "s|SABAPIKEY|$SABNZB_API_KEY|" \
	-e "s|SABURL|$SABURL|" \
	-e "s|SABMETHOD|$SABMETHOD|" \
	-e "s|SLACK_HOOK_URL|$SLACK_HOOK_URL|" \
	-e "s|SMTP_FROM|$SMTP_FROM|" \
	-e "s|SMTP_PORT|$SMTP_PORT|" \
	-e "s|SMTP_RELAY|$SMTP_RELAY|" \
	-e "s|SMTP_TO|$SMTP_TO|" \
	-e "s|TXPASS|$TRANSMISSION_PASS|" \
	-e "s|TXURL|$TXURL|" \
	-e "s|TXUSER|$TRANSMISSION_USER|" \
	-e "s|TXMETHOD|$TXMETHOD|" \
	/config.ini >/config/config.ini
    if test "$NEWZNAB_API_KEY"; then
	NEWZNAB_HOST=${NEWZNAB_HOST:-newznab}
	NEWZNAB_PROTO=${NEWZNAB_PROTO:-http}
	if test "$NEWZNAB_PORT"; then
	    NNPORT=:$NEWZNAB_PORT
	fi
	cat <<EOF >>/config/config.ini
[KUBENEWS]
kubenews = 1
kubenews_name = kubenews
kubenews_url = $NEWZNAB_PROTO://$NEWZNAB_HOST$NNPORT
kubenews_cat_ids = 5000, 5020, 5030, 5040, 5045, 5050, 5060, 5070, 5080
kubenews_api_key = $NEWZNAB_API_KEY
kubenews_search_mode = eponly
kubenews_search_fallback = 0
kubenews_enable_daily = 1
kubenews_enable_backlog = 1
kubenews_enable_manualsearch = 1
kubenews_enable_search_delay = 0
kubenews_search_delay = 60
EOF
    fi
fi

unset NEWZNAB_API_KEY SLACK_HOOK_URL NEWZNAB_HOST NNPORT NEWZNAB_PROTO TXPORT \
    SMTP_TO SMTP_FROM SMTP_RELAY DO_SLACK SABMETHOD SABNZB_PORT SABPORT SABURL \
    SABNZB_HOST SABNZB_API_KEY SABNZB_PROTO PROVIDERS DO_SMTP ADMIN_PASS TXURL \
    ADMIN_USER SABNZBD_PORT DISABLE_AUTH TRANSMISSION_USER TRANSMISSION_PASS \
    TRANSMISSION_PROTO TRANSMISSION_HOST TXMETHOD TRANSMISSION_PORT DO_TORRENT \
    DO_NZB DO_SAB DO_DEBUG

cd /app/medusa
exec python start.py --nolaunch --datadir /config
