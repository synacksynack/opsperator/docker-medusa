apiVersion: v1
kind: Template
labels:
  app: medusa
  template: medusa-persistent
message: |-
  The following service(s) have been created in your project:
      https://medusa.${ROOT_DOMAIN} -- Medusa
metadata:
  annotations:
    description: Medusa - persistent
    iconClass: icon-python
    openshift.io/display-name: Medusa
    tags: medusa
  name: medusa-persistent
objects:
- apiVersion: v1
  kind: PersistentVolumeClaim
  metadata:
    name: medusa-${FRONTNAME}
  spec:
    accessModes:
    - ReadWriteOnce
    resources:
      requests:
        storage: ${MEDUSA_VOLUME_CAPACITY}
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    labels:
      name: medusa-${FRONTNAME}
    name: medusa-${FRONTNAME}
  spec:
    replicas: 1
    selector:
      name: medusa-${FRONTNAME}
    strategy:
      type: Recreate
    template:
      metadata:
        labels:
          name: medusa-${FRONTNAME}
      spec:
        dnsPolicy: ClusterFirst
        containers:
        - env:
          - name: ADMIN_PASS
            valueFrom:
              secretKeyRef:
                key: admin-password
                name: medusa-${FRONTNAME}
          - name: ADMIN_USER
            valueFrom:
              secretKeyRef:
                key: admin-user
                name: medusa-${FRONTNAME}
          - name: API_KEY
            valueFrom:
              secretKeyRef:
                key: api-key
                name: medusa-${FRONTNAME}
          - name: SMTP_FROM
            value: medusa@${ROOT_DOMAIN}
          - name: SMTP_RELAY
            value: ${SMTP_RELAY}
          - name: SMTP_TO
            value: admin0@${ROOT_DOMAIN}
          - name: TZ
            value: Europe/Paris
          imagePullPolicy: IfNotPresent
          livenessProbe:
            initialDelaySeconds: 30
            httpGet:
              path: /
              port: 8080
            periodSeconds: 20
            timeoutSeconds: 8
          name: medusa
          ports:
          - containerPort: 8080
            protocol: TCP
          readinessProbe:
            initialDelaySeconds: 5
            httpGet:
              path: /
              port: 8080
            periodSeconds: 20
            timeoutSeconds: 5
          resources:
            limits:
              cpu: "${MEDUSA_CPU_LIMIT}"
              memory: "${MEDUSA_MEMORY_LIMIT}"
          volumeMounts:
          - mountPath: /config
            name: data
            subPath: config
          - mountPath: /download
            name: data
            subPath: download
          - mountPath: /tv
            name: data
            subPath: tv
          - mountPath: /anime
            name: data
            subPath: anime
        volumes:
        - name: data
          persistentVolumeClaim:
            claimName: medusa-${FRONTNAME}
    triggers:
    - type: ImageChange
      imageChangeParams:
        automatic: true
        containerNames:
        - medusa
        from:
          kind: ImageStreamTag
          name: medusa:${MEDUSA_IMAGE_TAG}
    - type: ConfigChange
- apiVersion: v1
  kind: Service
  metadata:
    name: medusa-${FRONTNAME}
  spec:
    ports:
    - name: http
      port: 8080
    selector:
      name: medusa-${FRONTNAME}
- apiVersion: v1
  kind: Route
  metadata:
    name: medusa-${FRONTNAME}
  spec:
    host: medusa.${ROOT_DOMAIN}
    to:
      kind: Service
      name: medusa-${FRONTNAME}
    tls:
      insecureEdgeTerminationPolicy: Redirect
      termination: edge
parameters:
- name: FRONTNAME
  description: The name identifier assigned to objects defined in this template
  displayName: Name
  required: true
  value: demo
- name: MEDUSA_CPU_LIMIT
  description: Maximum amount of CPU a Medusa container can use
  displayName: Lemon CPU Limit
  required: true
  value: 300m
- name: MEDUSA_IMAGE_TAG
  description: The ImageStreamTag we should pull images from
  displayName: Tag
  required: true
  value: master
- name: MEDUSA_MEMORY_LIMIT
  description: Maximum amount of memory a Medusa container can use
  displayName: Lemon Memory Limit
  required: true
  value: 1Gi
- name: MEDUSA_VOLUME_CAPACITY
  description: Volume space available for Medusa downloads
  displayName: Medusa Downloads Volume Capacity
  required: true
  value: 50Gi
- name: ROOT_DOMAIN
  description: Root Domain
  displayName: Root Domain
  required: true
  value: demo.local
- name: SMTP_RELAY
  description: SMTP Relay
  displayName: SMTP Relay
  required: true
  value: smtp.demo.local
